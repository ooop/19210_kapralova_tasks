#include "pch.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
using namespace std;

// *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***

class TokenService {
    string str;
    vector <string> words;
public:
    void init(string stroka) {
        words.clear();
        str = stroka;
    }
    vector <string> split() {
        string word;
        for (int i = 0; i <= str.size(); ++i) {
            if (str[i] >= 97 && str[i] <= 122 || str[i] >= 65 && str[i] <= 90 || str[i] >= 48 && str[i] <= 57)
                word += str[i];
            else {
                if (word.size() > 0)
                    words.push_back(word);
                word = "";
            }
        }
        return words;
    }
};

TEST(TokenService_test, split_test) {
   cout << endl << endl << "    ^^^***    TokenService tests:    ***^^^" << endl << endl;
    TokenService ob1;
    vector <string> words;
    string str;

    str = "()*";
    ob1.init(str);
    words = {};
    EXPECT_EQ(words, ob1.split());

    str = "hel!lo wor123ld9wor(lo) hel =-hel";
    ob1.init(str);
    words = { "hel", "lo", "wor123ld9wor", "lo", "hel", "hel"};
    EXPECT_EQ(words, ob1.split());

    str = "(lo) hel jou 125 125";
    ob1.init(str);
    words = {"lo", "hel", "jou", "125", "125"};
    EXPECT_EQ(words, ob1.split());

    str = "gvj7679#^&hgk 645 hhui*^$ kjiu66";
    ob1.init(str);
    words = {"gvj7679", "hgk", "645", "hhui", "kjiu66"};
    EXPECT_EQ(words, ob1.split());

    str = "!!!~p[165vft}dxd cgfcgtr @+KHD^";
    ob1.init(str);
    words = { "p", "165vft", "dxd", "cgfcgtr", "KHD" };
    EXPECT_EQ(words, ob1.split());

    //EXPECT_TRUE(true);
}

// *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***

class WordStateService {
    int count = 0;
    map <string, int> frequency;
    vector <pair<string, double>> prosent;
    vector < vector <string> > result;
    void createstate() {
        prosent.clear();
        map <string, int>::iterator it;
        for (it = frequency.begin(); it != frequency.end(); ++it)
            prosent.push_back(make_pair((*it).first, (((double)(*it).second / (double)count) * 100)));
        sort(prosent.begin(), prosent.end(), [](auto& left, auto& right) {
            return left.second > right.second;
            });
    }
public:
    map <string, int> accumulation(vector <string> words) {
        for (int i = 0; i < words.size(); ++i) {
            count++;
            if (frequency.find(words[i]) == frequency.end())
                frequency.insert(pair<string, int>(words[i], 1));
            else
                (frequency.find(words[i]))->second++;
        }
        return frequency;
    }

    vector < vector <string> > getresult() {
        createstate();
        result.clear();
        result.resize(3);
        for (int i = 0; i < prosent.size(); ++i) {
            result[0].push_back(prosent[i].first);
            result[1].push_back(to_string(prosent[i].second));
            result[2].push_back(to_string((*frequency.find(prosent[i].first)).second));
        }
        return result;
    }
};

TEST(WordStateService_test, accumulation_test) {
    cout << endl << endl << "   ^^^***    WordStateService tests (accumulation):    ***^^^" << endl << endl;
    WordStateService ob1;
    vector <string> words;
    map <string, int> frequency;

    words = {};
    frequency = {};
    EXPECT_EQ(frequency, ob1.accumulation(words));

    words = { "p", "165vft", "dxd", "cgfcgtr", "KHD"};
    frequency = { {"p", 1}, {"165vft", 1}, {"dxd", 1}, {"cgfcgtr", 1}, {"KHD", 1} };
    EXPECT_EQ(frequency, ob1.accumulation(words));

    words = { "p", "dxd", "KHD", "hhui" };
    frequency = { {"p", 2}, {"165vft", 1}, {"dxd", 2}, {"cgfcgtr", 1}, {"KHD", 2}, {"hhui", 1} };
    EXPECT_EQ(frequency, ob1.accumulation(words));

    words = {"gvj7679", "165vft", "645", "hhui", "dxd"};
    frequency = { {"p", 2}, {"165vft", 2}, {"dxd", 3}, {"cgfcgtr", 1}, {"KHD", 2}, {"hhui", 2}, {"gvj7679", 1}, {"645", 1} };
    EXPECT_EQ(frequency, ob1.accumulation(words));
}

TEST(WordStateService_test, getresult_test) {
    cout << endl << endl << "   ^^^***    WordStateService tests (getresult):    ***^^^" << endl << endl;
    WordStateService ob1;
    vector <string> words;
    vector < vector <string> > result;

    words = {};
    ob1.accumulation(words);
    result = { {}, {}, {} };
    EXPECT_EQ(result, ob1.getresult());

    words = { "p", "165vft", "dxd", "cgfcgtr", "KHD" };
    ob1.accumulation(words);
    result = { {"165vft", "KHD", "cgfcgtr", "dxd", "p"}, {"20.000000", "20.000000", "20.000000", "20.000000", "20.000000"}, {"1", "1", "1", "1", "1"} };
    EXPECT_EQ(result, ob1.getresult());

    words = { "p", "dxd", "KHD", "hhui" };
    ob1.accumulation(words);
    result = { { "KHD", "dxd", "p", "165vft", "cgfcgtr", "hhui" }, { "22.222222", "22.222222", "22.222222", "11.111111", "11.111111", "11.111111" }, { "2", "2", "2", "1", "1", "1" } };
    EXPECT_EQ(result, ob1.getresult());

    words = { "gvj7679", "165vft", "645", "hhui", "dxd" };
    ob1.accumulation(words);
    result = { { "dxd", "165vft", "KHD", "hhui", "p", "645", "cgfcgtr", "gvj7679" }, { "21.428571", "14.285714", "14.285714", "14.285714", "14.285714", "7.142857", "7.142857", "7.142857" }, { "3", "2", "2", "2", "2", "1", "1", "1" } };
    EXPECT_EQ(result, ob1.getresult());
}