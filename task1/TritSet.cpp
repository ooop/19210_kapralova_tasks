#include "TritSet.h"
#include "Trit.h"

TritSet::TritSet(uint trits_count) {
<<<<<<< HEAD
    this->size = trits_count / 4 + 1;
    trits = new uint[arrayTritsLength()];
    for (int i = 0; i < arrayTritsLength(); ++i)
        trits[i] = 2863311530;
}
TritSet::TritSet(const TritSet& other) {
    this->size = other.size;
    trits = new uint[this->arrayTritsLength()];
    for (int i = 0; i < arrayTritsLength(); ++i)
=======
    size = trits_count / (4 * uint_sizeof) + (trits_count % (4 * uint_sizeof) ? 1 : 0);
    trits = new uint[size];
    for (int i = 0; i < size; ++i)
        trits[i] = 2863311530; 
}
TritSet::TritSet(const TritSet& other) {
    size = other.size;
    trits = new uint[size];
    for (int i = 0; i < size; ++i)
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
        this->trits[i] = other.trits[i];
}
TritSet::TritSet() {
    this->size = 0;
    this->trits = nullptr;
}


size_t TritSet::length() const {
<<<<<<< HEAD
    for (uint i = arrayTritsLength() * uint_sizeof * 4 - 1; i > 0; i--)
=======
    for (uint i = capacity() - 1; i > 0; i--)
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
        if (getTritValue(i) != UNKNOWN)
            return i + 1;
    return 1;
}


Trit TritSet::operator[] (int index) const {
    Trit t(index, this);
    return t;
}


const TritSet TritSet::operator&(const TritSet& ts) const {
<<<<<<< HEAD
    TritSet new_set(this->arrayTritsLength() >= ts.arrayTritsLength() ? *this : ts);
    uint N = this->arrayTritsLength() >= ts.arrayTritsLength() ? ts.arrayTritsLength() : this->arrayTritsLength();
    for (uint i = 0; i < N * uint_sizeof * 4; ++i)
=======
    TritSet new_set(this->size >= ts.size ? *this : ts);
    uint N = this->capacity() >= ts.capacity() ? ts.capacity() : this->capacity();
    for (uint i = 0; i < N; ++i)
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
        new_set.setTrit(i, ((*this)[i] & ts[i]));
    return std::move(new_set);
}

const TritSet TritSet::operator|(const TritSet& ts) const {
<<<<<<< HEAD
    TritSet new_set(this->arrayTritsLength() >= ts.arrayTritsLength() ? *this : ts);
    uint N = this->arrayTritsLength() >= ts.arrayTritsLength() ? ts.arrayTritsLength() : this->arrayTritsLength();
    for (uint i = 0; i < N * uint_sizeof * 4; ++i)
=======
    TritSet new_set(this->size >= ts.size ? *this : ts);
    uint N = this->capacity() >= ts.capacity() ? ts.capacity() : this->capacity();
    for (uint i = 0; i < N; ++i)
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
        new_set.setTrit(i, ((*this)[i] | ts[i]));
    return std::move(new_set);
}

const TritSet TritSet::operator!() const {
    TritSet new_set(*this);
    for (int i = 0; i < capacity(); ++i)
        new_set.setTrit(i, (!(*this)[i]));
    return std::move(new_set);
}


TritSet& TritSet::operator=(const TritSet& other) {
    this->size = other.size;
    delete this->trits;
<<<<<<< HEAD
    trits = new uint[this->arrayTritsLength()];
=======
    trits = new uint[this->size];
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
    for (uint i = 0; i < capacity(); i++)
        (*this)[i] = other[i];
    return *this;
}

TritSet& TritSet::operator&=(const TritSet& other) {
    *this = (*this & other);
    return *this; 
}

TritSet& TritSet::operator|=(const TritSet& other) {
    this->size = other.size;
    delete this->trits;
<<<<<<< HEAD
    trits = new uint[this->arrayTritsLength()];
=======
    trits = new uint[this->size];
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
    for (uint i = 0; i < capacity(); i++)
        (*this)[i] |= other[i];
    return *this;
}


uint TritSet::cardinality(TritValue tv) const {
    uint count_trits = 0;
    for (uint i = capacity() - 1; i > 0; i--) {
        if (getTritValue(i) == tv)
            count_trits++;
    }
    return count_trits;
}

void TritSet::trim(size_t lastIndex) {
    for (int i = lastIndex; i < lastIndex + uint_sizeof * 4; ++i)
        this->setTrit(i, UNKNOWN);
    uint byte_number = lastIndex / 4; 
    uint array_position = byte_number / uint_sizeof + 1;
<<<<<<< HEAD
    for (int i = array_position; i < arrayTritsLength(); ++i)
=======
    for (int i = array_position; i < size; ++i)
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
        trits[i] = 2863311530;
}

void TritSet::shrink() {
<<<<<<< HEAD
    uint new_size = (length() - 1) / 4 + 1;
    uint* trits1 = new uint[new_size / uint_sizeof + (new_size % uint_sizeof ? 1 : 0)];
    for (int i = 0; i < new_size / uint_sizeof + (new_size % uint_sizeof ? 1 : 0); ++i)
=======
    uint new_size = (length() - 1) / (4 * uint_sizeof) + ((length() - 1) % (4 * uint_sizeof) ? 1 : 0);
    uint* trits1 = new uint[new_size];
    for (int i = 0; i < new_size; ++i)
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
        trits1[i] = trits[i];
    delete trits;
    trits = trits1;
    size = new_size;
}



TritValue TritSet::getTritValue(uint trit_index) const {
<<<<<<< HEAD
    uint byte_index = trit_index / 4; //������ �����
    uint array_index = byte_index / uint_sizeof; //������ uint 
    uint array_elem = this->trits[array_index];
    uint first_position = (trit_index - 4 * uint_sizeof * (array_index)) * 2; //������� ���� � uint
=======
    uint byte_index = trit_index / 4; //индекс байта
    uint array_index = byte_index / uint_sizeof; //индекс uint 
    uint array_elem = this->trits[array_index];
    uint first_position = (trit_index - 4 * uint_sizeof * (array_index)) * 2; //позиция бита в uint
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
    return createTrirValue(getBit(first_position, array_elem), getBit(first_position + 1, array_elem));
}

TritValue TritSet::createTrirValue(bool first_bit, bool second_bit) const {
    if (first_bit == 1 && second_bit == 1)
        return TRUE;
    else if (first_bit == 0 && second_bit == 0)
        return FALSE;
    else return UNKNOWN;
}

TritValue TritSet::smartGetTritValue(int index) const {
    if (isMore(index))
        return UNKNOWN;
    return this->getTritValue(index);
}



<<<<<<< HEAD
void TritSet::setTrit(uint trit_index, TritValue trit) {
    uint byte_index = trit_index / 4; //������ �����
    uint array_index = byte_index / uint_sizeof; //������ uint 
    uint first_position = (trit_index - 4 * uint_sizeof * (array_index)) * 2; //������� � uint
    if (trit == TRUE) {
=======
void TritSet::setTrit(uint trit_index, TritValue tv) {
    uint byte_index = trit_index / 4; //индекс байта
    uint array_index = byte_index / uint_sizeof; //индекс uint 
    uint first_position = (trit_index - 4 * uint_sizeof * (array_index)) * 2; //позиция в uint
    if (tv == TRUE) {
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
        trits[array_index] = std::bitset<uint_sizeof* CHAR_BIT>(trits[array_index]).set(uint_sizeof * 8 - first_position - 1, true).to_ulong();
        trits[array_index] = std::bitset<uint_sizeof* CHAR_BIT>(trits[array_index]).set(uint_sizeof * 8 - first_position - 2, true).to_ulong();
    }
    else {
<<<<<<< HEAD
        if (trit == FALSE)
=======
        if (tv == FALSE)
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
            trits[array_index] = std::bitset<uint_sizeof* CHAR_BIT>(trits[array_index]).set(uint_sizeof * 8 - first_position - 1, false).to_ulong();
        else
            trits[array_index] = std::bitset<uint_sizeof* CHAR_BIT>(trits[array_index]).set(uint_sizeof * 8 - first_position - 1, true).to_ulong();
        trits[array_index] = std::bitset<uint_sizeof* CHAR_BIT>(trits[array_index]).set(uint_sizeof * 8 - first_position - 2, false).to_ulong();
    }
}

void TritSet::takeMoreMemory(int index) {
<<<<<<< HEAD
    int size_new = index / 4 + 1;
    uint* trits1 = new uint[size_new / uint_sizeof + (size_new % uint_sizeof ? 1 : 0)];
    for (int i = 0; i < size / uint_sizeof; ++i)
        trits1[i] = trits[i];
    delete trits;
    trits = trits1;
    size = size_new;
=======
    int new_size = index / (4 * uint_sizeof) + (index % (4 * uint_sizeof) ? 1 : 0);;
    uint* trits1 = new uint[new_size / (4 * uint_sizeof) + (new_size % (4 * uint_sizeof) ? 1 : 0)];
    for (int i = 0; i < size; ++i)
        trits1[i] = trits[i];
    delete trits;
    trits = trits1;
    size = new_size;
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
}

void TritSet::smartSetTrit(int index, TritValue tv) {
    if (index == -1)
        return;
    if (isMore(index) && tv != UNKNOWN) {
        this->takeMoreMemory(index);
        this->setTrit(index, tv);
    }
    else
        if (!isMore(index))
            this->setTrit(index, tv);
<<<<<<< HEAD
}
=======
}
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
