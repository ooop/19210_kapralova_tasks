#pragma once
#include "includes.h"

class Trit;
class TritSet {
<<<<<<< HEAD
    uint* trits;        // ������ ������
    uint size;    // ������� ���� ����� ��� �������� �������

    uint arrayTritsLength() const { return size / uint_sizeof + (size % uint_sizeof ? 1 : 0); }
    // ����� ������� trits � uint,

    void setTrit(unsigned int trit_index, TritValue trit); // ���������� ����
    void takeMoreMemory(int index);              //��������� ������ trits
    bool isMore(int index) const { return index + 1 > size * 4; }  //���� ������ ������ ����� ������ � ����������
    void smartSetTrit(int index, TritValue tv);

    TritValue getTritValue(uint trit_index) const;              // ����� �������� �����
    bool getBit(uint position, uint elem) const { return (elem >> (uint_sizeof * 8 - position - 1)) & 1; }
    TritValue createTrirValue(bool first_bit, bool second_bit) const; //������������� ���� �� ���� �����
=======
    uint* trits;        // массив тритов
    uint size;

    void setTrit(unsigned int trit_index, TritValue trit); // установить трит
    void takeMoreMemory(int index);              //увеличить размер trits
    bool isMore(int index) const { return index + 1 > capacity(); }  //если индекс больше числа тритов в контейнере
    void smartSetTrit(int index, TritValue tv);

    TritValue getTritValue(uint trit_index) const;              // взять значение трита
    bool getBit(uint position, uint elem) const { return (elem >> (uint_sizeof * 8 - position - 1)) & 1; }
    TritValue createTrirValue(bool first_bit, bool second_bit) const; //сгенерировать трит из двух битов
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec
    TritValue smartGetTritValue(int index) const;

public:
    friend class Trit;
<<<<<<< HEAD
    TritSet(uint trits_count);     // ����������� �������� ������ ��� trits_count ������
    TritSet();
    TritSet(const TritSet& other);      //����������� ����������� 

    size_t capacity() const { return arrayTritsLength() * uint_sizeof * 4; }    // ������ ������ � ������
    size_t length() const;                  //���������� �������� ������
=======
    TritSet(uint trits_count);     // конструктор выделяет память под trits_count тритов
    TritSet();
    TritSet(const TritSet& other);      //конструктор копирования 

    size_t capacity() const { return size * uint_sizeof * 4; }    // размер памяти в тритах
    size_t length() const;                  //количество значащих тритов
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec

    const TritSet operator&(const TritSet& ts) const;
    const TritSet operator|(const TritSet& ts) const;
    const TritSet operator!() const;

    TritSet& operator=(const TritSet& other);
    TritSet& operator&=(const TritSet& other);
    TritSet& operator|=(const TritSet& other);
    Trit operator[](int index) const;

<<<<<<< HEAD
    uint cardinality(TritValue tv) const;   // ������� ����� ������
    void trim(size_t lastIndex);            // ������ ���������� �� lastIndex � ������
    void shrink();                          //������������ ������ ����� ���������� �������������� �����
=======
    uint cardinality(TritValue tv) const;   // сколько таких тритов
    void trim(size_t lastIndex);            // забыть содержимое от lastIndex и дальше
    void shrink();                          //освобождение памяти после последнего установленного трита
>>>>>>> 9d07706867460587c430b2bf268c7a04577bb0ec

};
